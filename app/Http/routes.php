<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'web'], function () {
	Route::auth();
	Route::get('/', 'HomeController@index');
	Route::get('/register', function () {
    return redirect('/');
	});
	Route::post('/register', function () {
    return redirect('/');
	});
});

Route::group(['middleware' => 'admin'], function () {
	Route::get('/admin', 'AdminController@index');
	Route::get('/admin/gods', 'AdminController@gods');
	Route::post('/admin/gods', 'AdminController@PostGods');
	Route::get('/admin/god/{id}', 'AdminController@editGod');
	Route::patch('/admin/god/{id}', 'AdminController@postEditGod');
	Route::get('/admin/god/message/{id}', 'AdminController@editGodMessage');
	Route::patch('/admin/god/message/{id}', 'AdminController@postEditGodMessage');
	Route::get('admin/list', 'AdminController@donationsList');
	Route::post('admin/list', 'AdminController@postDonationsList');
	Route::delete('admin/reset/{id}', 'AdminController@reset');
	Route::get('/admin/history', 'AdminController@history');
	Route::delete('/admin/history/{id}', 'AdminController@delete');
});