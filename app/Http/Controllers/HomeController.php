<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Donation;
use App\God;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gods = God::all();
        $donations = Donation::selectRaw('sum(amount) as amount_sum, god_id')
        ->join('gods', 'gods.id', '=', 'donations.god_id')
        ->groupBy('god_id')
        ->orderBy('amount_sum', 'desc')
        ->orderBy('gods.name', 'asc')
        ->get();
        return view('index', ['donations' => $donations, 'gods' => $gods]);
    }
}