<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\God;
use App\Donation;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
    	return view('admin.index');
    }

    public function gods()
    {
    	$gods = God::all();
    	return view('admin.gods', ['gods' => $gods]);
    }

    public function donationsList()
    {
    	$gods = God::all();
    	$donations = Donation::selectRaw('sum(amount) as amount_sum, god_id')
    	->join('gods', 'gods.id', '=', 'donations.god_id')
    	->groupBy('god_id')
    	->orderBy('amount_sum', 'desc')
    	->orderBy('gods.name', 'asc')
    	->get();
    	return view('admin.list', ['gods' => $gods, 'donations' => $donations]); 
    }

    public function postDonationsList(Requests\DonationsRequest $request)
    {
    	foreach($request->god_id as $god_id)
    	{
    		$donation = new Donation($request->all());
    		$donation->god_id = $god_id;
    		$donation->save();
    	}
    	
    	return redirect('/admin/list');
    }

    public function PostGods(Request $request)
    {
    	$god = new God($request->all());
    	$god->save();
    	return redirect('/admin/gods');
    }

    public function editGod($id)
    {
    	$god = God::find($id);
    	return view('admin.edit_god', ['god' => $god]);
    }

    public function editGodMessage($id)
    {
        $god = God::find($id);
        return view('admin.edit_god_message', ['god' => $god]);
    }

    public function reset($id)
    {
    	$donations = Donation::where('god_id', $id)->delete();
    	return redirect('/admin/list');
    }

    public function postEditGod(Request $request, $id)
    {
    	$god = God::find($id);
    	$god->name = $request->name;
    	$god->role = $request->role;
    	$god->avatar_url = $request->avatar_url;
    	$god->infos = $request->infos;
    	$god->save();
    	return redirect('/admin/gods');
    }

    public function postEditGodMessage(Request $request, $id)
    {
        $god = God::find($id);
        $god->infos = $request->infos;
        $god->save();
        return redirect('/');
    }

    public function history()
    {
    	$donations = Donation::orderBy('created_at', 'desc')->paginate(25);
    	return view('admin.history', ['donations' => $donations]);
    }

    public function delete($id)
    {
    	$donation = Donation::find($id);
    	$donation->delete();
    	return redirect('/admin/history');
    }
}
