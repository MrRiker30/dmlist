<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    protected $fillable = [
        'amount', 'god_id'
    ];
    public function god()
    {
    	return $this->belongsTo('App\God');
    }
    
}
