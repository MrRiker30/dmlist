<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class God extends Model
{
    protected $fillable = [
        'name', 'role', 'avatar_url', 'infos'
    ];
    public function donation()
    {
    	return $this->hasMany('App\Donation');
    }
    
}
