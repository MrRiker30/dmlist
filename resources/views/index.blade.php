@extends('app')
@section('content')

@if(Auth::check())
@if(Auth::user()->type == 'A')
<div class="alert alert-info">
  <strong>Mercury Tips : </strong> To enter a donation super fast : 
  <br>
  Enter the amount of the donation<br>
  Press <kbd>Tab</kbd><br>
  Write god name (use arrows and the <kbd>Enter Key</kbd> to chose one)<br>
  Press <kbd>Enter</kbd>
</div>
<h2>Add a new donation</h2>
{!! Form::open(array('method' => 'post', 'class' => 'form-horizontal', 'url' => '/admin/list', )) !!}
    <div class="form-group">
        <div class="col-sm-6 col-xs-12 col-sm-offset-3">
            <div class="form-material">
                {{ Form::text('amount', null, array('class' => 'form-control', 'placeholder' => 'Amount', 'autofocus')) }}  
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6 col-xs-12 col-sm-offset-3">
            <select name="god_id[]" class="chosen form-control" data-placeholder="Chose gods" data-live-search="true" multiple="multiple">
                @foreach($gods as $god)
                    <option value="{!!$god->id!!}">{{ $god->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group text-center">
        <div class="col-xs-12">
            <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-plus push-5-r"></i> Add donation</button>
        </div>
    </div>
{{ Form::close() }}
@endif
@endif
<h2>Donations List</h2>
<div class="row">
<?php $nb = 0; $amount = 0; ?>
    <div class="col-sm-12">
    <table class="table table-striped table-hover ">
      @if(Auth::check())
      @if(Auth::user()->type == 'A')
      <thead>
        <tr>
          <th>#</th>
          <th>Amount</th>
          <th>God</th>
          <th>Infos</th>
          <th>Reset</th>
        </tr>
      </thead>
      @endif
      @else
      <thead>
        <tr>
          <th>#</th>
          <th>Amount</th>
          <th>God</th>
          <th>Infos</th>
        </tr>
      </thead>
      @endif
      <tbody>
      @foreach($donations as $donation)
      @if(Auth::check())
      @if(Auth::user()->type == 'A')
        <tr>
          <td><?php if($amount == $donation->amount_sum){++$nb; echo '<i class="fa fa-minus"></i>';} else {++$nb; echo $nb;} $amount = $donation->amount_sum ?></td>
          <td>${{ $donation->amount_sum }}</td>
          <td><img src="{{ $donation->god->avatar_url }}" width="30"> {{ $donation->god->name }} - <a href="/admin/god/message/{{ $donation->god->id }}">Edit Infos</a></td>
          <td>{{$donation->god->infos}}</td>
          <td>{{ Form::open(['method' => 'delete', 'action' => array('AdminController@reset', $donation->god->id )]) }}
            <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-refresh"></i> Reset</button>
            {{ Form::close() }}</td>
        </tr>
        @endif
        @else
        <tr>
          <td><?php if($amount == $donation->amount_sum){++$nb; echo '<i class="fa fa-minus"></i>';} else {++$nb; echo $nb;} $amount = $donation->amount_sum ?></td>
          <td>${{ $donation->amount_sum }}</td>
          <td><img src="{{ $donation->god->avatar_url }}" width="30"> {{ $donation->god->name }}</td>
          <td>{{$donation->god->infos}}</td>
        </tr>
        @endif
      @endforeach
        </tbody>
      </table>
    </div>
</div>
@stop