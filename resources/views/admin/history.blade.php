@extends('app')
@section('content')
<h2>Donations history</h2>
<table class="table table-striped table-hover ">
    <thead>
        <tr>
          <th>#</th>
          <th>Amount</th>
          <th>God</th>
          <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        @foreach($donations as $donation)
        <tr>
          <td>{{ $donation->id }}</td>
          <td><b>${{ $donation->amount }}</b></td>
          <td>{{ $donation->god->name }}</td>
          <td>
          {{ Form::open(['method' => 'delete', 'action' => array('AdminController@delete', $donation->id )]) }}
          <button role="submit" class="btn btn-danger">Delete</button>
          {{ Form::close() }}
          </td>
        </tr>
        @endforeach
    </tbody>
</table>
{!! $donations->links() !!}
@stop