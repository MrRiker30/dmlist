@extends('app')
@section('content')
 {!! Form::model($god, array('method' => 'PATCH', 'class' => 'form-horizontal')) !!}
 	<div class="form-group">
 		<a href="/admin/gods" class="btn btn-primary"><i class="fa fa-long-arrow-left"></i> Back</a>
 	</div>
    <div class="form-group">
        <div class="col-xs-12">
            <div class="form-material">
                {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'God name')) }}  
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <div class="form-material">
                {{ Form::select('role', array('mage' => 'Mage', 'hunter' => 'Hunter', 'warrior' => 'Warrior', 'guardian' => 'Guardian', 'assassin' => 'Assassin'), $god->role , array('class' => 'form-control')) }}
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <div class="form-material">
                {{ Form::text('avatar_url', null, array('class' => 'form-control', 'placeholder' => 'Image Url')) }}  
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <div class="form-material">
                {{ Form::textarea('infos', null, array('class' => 'form-control', 'placeholder' => 'Skins, Items, all the stuff asked by the donator')) }}  
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-plus push-5-r"></i> Update God</button>
        </div>
    </div>
{{ Form::close() }}
@stop