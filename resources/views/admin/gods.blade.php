@extends('app')
@section('content')
<h2>Add a God</h2>
{!! Form::open(array('class' => 'form-horizontal')) !!}
    <div class="form-group">
        <div class="col-sm-6 col-xs-12 col-sm-offset-3">
            <div class="form-material">
                {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'God name')) }}  
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6 col-xs-12 col-sm-offset-3">
            <div class="form-material">
                {{ Form::select('role', array('mage' => 'Mage', 'hunter' => 'Hunter', 'warrior' => 'Warrior', 'guardian' => 'Guardian', 'assassin' => 'Assassin'), 'Mage', array('class' => 'form-control')) }}
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6 col-xs-12 col-sm-offset-3">
            <div class="form-material">
                {{ Form::text('avatar_url', null, array('class' => 'form-control', 'placeholder' => 'Image Url')) }}  
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6 col-xs-12 col-sm-offset-3 text-center">
            <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-plus push-5-r"></i> Add god</button>
        </div>
    </div>
{{ Form::close() }}
<h2>Gods List</h2>
<div class="row">
@foreach($gods as $god)
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
        <div class="panel panel-default">
          <div class="panel-body">
            <a href="/admin/god/{{ $god->id }}"><img width="60" src="{{ $god->avatar_url }}" data-toggle="tooltip" data-placement="top" title="{{ $god->name }}"></a>
          </div>
        </div>
    </div>
@endforeach
</div>
@stop