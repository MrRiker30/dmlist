@extends('app')
@section('content')
 {!! Form::model($god, array('method' => 'PATCH', 'class' => 'form-horizontal')) !!}
 	<div class="form-group">
 		<a href="/" class="btn btn-primary"><i class="fa fa-long-arrow-left"></i> Back</a>
 	</div>
    <div class="form-group">
        <div class="col-xs-12">
            <div class="form-material">
                {{ Form::textarea('infos', null, array('class' => 'form-control', 'placeholder' => 'Skins, Items, all the stuff asked by the donator')) }}  
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <button class="btn btn-sm btn-success" type="submit"><i class="fa fa-plus push-5-r"></i> Update God</button>
        </div>
    </div>
{{ Form::close() }}
@stop