# DM Donations List

DM Donations List is a web application for easier management of the donations list for dmbrandon's stream.

## Install

This web app was made with the Laravel Framework.
Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
